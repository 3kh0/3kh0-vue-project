import { createRouter, createWebHistory } from 'vue-router';

// Import pages here
import index from './views/index.vue';
import notFound from './views/notFound.vue';
// extra pages here
import extraIndex from './views/extraIndex.vue';
import fursonaRef from './views/fursonaRef.vue'
import crypto from './views/crypto.vue'
import aboutblank from './views/aboutblank.vue'
import iplist from './views/iplist.vue'
import urlopen from './views/urlopen.vue'

const routes = [
  // system pages
  { path: '/', component: index },
  // extra pages
  { path: '/extra', component: extraIndex },
  { path: '/extra/index', component: extraIndex},
  { path: '/extra/fursona-ref', component: fursonaRef },
  { path: '/extra/crypto', component: crypto },
  { path: '/extra/aboutblank', component: aboutblank },
  { path: '/extra/iplist', component: iplist },
  { path: '/extra/urlopen', component: urlopen },
  // catch-all route
  { path: '/:pathMatch(.*)*', component: notFound },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router